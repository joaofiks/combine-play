import UIKit
import Combine

let weatherPublisher = PassthroughSubject<Int, Never>()

let subscriber = weatherPublisher
    .filter {$0 > 25}
    .sink {value in
        print("\(value) is greater than 25")
}

let completeSubscriber = weatherPublisher.handleEvents(receiveSubscription: {subscription in
    print("New Subscription: \(subscription)")
}, receiveOutput: {output in
    print("New Output : \(output)")
}, receiveCompletion: {error in
    print("Subscription Completed with error: \(error)")
}, receiveCancel: {
    print("Subscription Canceled")
}).sink{ value in
    print("Subscriber received value:\(value)")
}

weatherPublisher.send(16)
weatherPublisher.send(18)
weatherPublisher.send(20)
weatherPublisher.send(22)
weatherPublisher.send(24)
weatherPublisher.send(26)
weatherPublisher.send(28)
weatherPublisher.send(30)
